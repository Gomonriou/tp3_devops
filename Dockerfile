FROM node:12-alpine

ENV NODE_ENV development
ENV PORT=8080

USER root

WORKDIR /app
COPY package*.json ./

RUN apk update && apk add sqlite

RUN npm install 

COPY . .
EXPOSE 8080
CMD ["node","app.js"]
